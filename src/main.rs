mod brainfuck;
mod util;

use std::env;
use std::error::Error;
use std::fs::File;
use std::io::{self};
use std::io::Read;
use std::path::Path;

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("=== Brainfuck ===");

    if args.len() > 1 {
        let path = Path::new(&args[1]);
        let mut file = match File::open(&path) {
            Ok(file) => file,
            Err(why) => panic!("couldn't open {:?}: {}", path, why.description()),
        };

        let mut content = String::new();
        match file.read_to_string(&mut content) {
            Ok(_) => {
                // Run
                let code = util::string_to_tape(content);
                let mut state = brainfuck::State::new(code);
                state.run();
            },
            Err(why) => println!("couldn't read {:?}: {}", path, why.description()),
        }
    } else {
        let mut code = String::new();
        println!("Program input (+-.,<>[]):");
        io::stdin().read_line(&mut code).expect("could not read a line");
        let code = util::string_to_tape(code);
        let mut state = brainfuck::State::new(code);
        state.run();
    }
}