use std::io::BufRead;
use std::io::{self};

use brainfuck;

pub fn inc(c: char) -> char {
    let u = c as u8;
    if u == 255 {
        0u8 as char
    } else {
        (u + 1) as char
    }
}

pub fn dec(c: char) -> char {
    let u = c as u8;
    if u == 0 {
        255u8 as char
    } else {
        (u - 1) as char
    }
}

pub fn string_to_tape(s: String) -> brainfuck::Tape {
    s.chars().collect()
}

/// Abstraction for a buffer for input
/// so it is possible to read char by char.
/// Otherwise not possible.
pub struct InputBuffer {
    buffer: String,
}

impl InputBuffer {
    pub fn new() -> InputBuffer {
        InputBuffer {
            buffer: String::new(),
        }
    }

    pub fn read_char(&mut self) -> Option<char> {
        if self.buffer.len() == 0 {
            self.read_line();
        }
        let character = self.buffer.chars().next();
        self.buffer = self.buffer[1..].to_string();  // Discarding the first char from buffer
        character
    }

    pub fn read_line(&mut self) {
        let stdin = io::stdin();
        let mut handle = stdin.lock();
        handle.read_line(&mut self.buffer).expect("Could not read a line");
    }
}