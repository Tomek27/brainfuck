use util;

pub struct State {
    pub code: Tape,
    pub tape: Tape,
    pub code_pos: usize,
    pub tape_pos: usize,
}

pub type Tape = Vec<char>;

impl State {
    pub fn new(code: Tape) -> State {
        Self::with_tape(code, vec![])
    }

    pub fn with_tape(code: Tape, tape: Tape) -> State {
        Self::with_positions(code, tape, 0, 0)
    }

    pub fn with_positions(code: Tape, tape: Tape, code_pos: usize, tape_pos: usize) -> State {
        State {
            code,
            tape,
            code_pos,
            tape_pos,
        }
    }

    pub fn run(&mut self) -> bool {
        let mut buffer = util::InputBuffer::new();
        self.run_skip(false, &mut buffer)
    }

    pub fn run_skip(&mut self, skip: bool, buffer: &mut util::InputBuffer) -> bool {
        while self.code_pos < self.code.len() {
            if self.tape_pos >= self.tape.len() {
                self.tape.push('\0');
            }

            if self.code[self.code_pos] == '[' {
                self.code_pos = self.code_pos + 1;
                let old_pos = self.code_pos;
                let character = self.tape[self.tape_pos];
                while self.run_skip(character == '\0', buffer) {
                    self.code_pos = old_pos;
                }
            } else if self.code[self.code_pos] == ']' {
                return self.tape[self.tape_pos] != '\0';
            } else if !skip {
                match self.code[self.code_pos] {
                    '+' => self.tape[self.tape_pos] = util::inc(self.tape[self.tape_pos]),
                    '-' => self.tape[self.tape_pos] = util::dec(self.tape[self.tape_pos]),
                    '>' => self.tape_pos = self.tape_pos + 1,
                    '<' => self.tape_pos = self.tape_pos - 1,
                    '.' => print!("{}", &self.tape[self.tape_pos]),
                    ',' => self.tape[self.tape_pos] = buffer.read_char().expect("Could not read char"),
                    _ => (),
                }
            }

            self.code_pos = self.code_pos + 1;
        }

        true
    }
}
