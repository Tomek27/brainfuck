# Brainfuck Interpreter
This is a simple [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck) interpreter 
taking either a text file with a valid Brainfuck program,
or prompts the user to input a program.

It is a [Rust](https://en.wikipedia.org/wiki/Rust_(programming_language)) re-implementation
of a Brainfuck interpreter described in [this blog](http://howistart.org/posts/nim/1/),
which has been written in [Nim](https://en.wikipedia.org/wiki/Nim_(programming_language)).